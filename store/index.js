export const state = () => ({
  authenticated: false,
  jwt: null
})

export const mutations = {
  setAuth (state, { auth, jwt }) {
    state.authenticated = auth
    state.jwt = jwt
  }
}

export const actions = {
  login ({ commit }, jwt) {
    localStorage.setItem('auth', JSON.stringify({ auth: true, jwt }))
    commit('setAuth', { auth: true, jwt })
  },
  logout ({ commit }) {
    localStorage.removeItem('auth')
    commit('setAuth', { auth: false , jwt: null })
  },
  initializeAuth ({ commit }) {
    const auth = JSON.parse(localStorage.getItem('auth'))
    if (auth && auth.auth) {
      commit('setAuth', { auth: true, user: auth.jwt })
    }
  }
}
